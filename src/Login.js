import React from 'react'  
import axiosInstance from "./Api/axios" 
import {FaUsers, FaUserTie} from 'react-icons/fa'
import LoginAlert from './LoginAlert'

class Login extends React.Component {
    constructor(props) {
        super(props)  
        this.state = {
        email: "",
        password: "",
        isError:'',
        isLogin:false
        }  
    }

    handleOnChange = (event) => {
        const { name, value } = event.target  
        this.setState({
        [name]: value,
        })  
    }  

    handleOnSubmit = (event) => {
        event.preventDefault()  
        const formData = {}
        formData['email'] = this.state.email
        formData['password'] = this.state.password  
        axiosInstance
        .post("/admin", formData)
        .then((response) => {
            const { token } = response.data  
            localStorage.setItem("token", token)
            this.setState({
                isError : false,
                isLogin:true
            })  
        })
        .catch((error) =>
            this.setState({
                isError:true,
                isLogin:false
            })
        )  
    }  

    render() {
        return (
            <div className='container-fluid'>
                <div className='col-sm-12 custom-bg rounded'>
                    <h1 className='text-white p-2'><FaUsers className='mb-1'style={{color:'gray'}}/> Employee</h1>
                </div>
                <form onSubmit = {this.handleOnSubmit} className = 'mx-auto mt-5 text-left p-3 custom-bg col-sm-5 rounded'>
                    <h2 className='text-white'><FaUserTie className='mb-1'style={{color:'gray'}}/> Admin Login</h2>
                    <div className = 'form-group'>
                        <label>Email :</label>
                        <input className = 'form-control' type = 'email' name = 'email'  onChange = {this.handleOnChange} required pattern="[a-z0-9._+%-]+@[a-z0-9]+\.[a-z]{2,}$"/>
                    </div>
                    <div className = 'form-group'>
                        <label>Password :</label>
                        <input className = 'form-control' type = 'password' name = 'password'  onChange = {this.handleOnChange} required pattern = ".{8,}"/>
                    </div>
                    <button type = 'submit' className = 'btn btn-secondary mt-2'>Submit</button>
                </form>
                {this.state.isError === true ? <LoginAlert Message = 'Unable to Login! Check the Details' login={this.state.isLogin} {...this.props} /> : (this.state.isError===false&&<LoginAlert Message = 'Login Successful!' login={this.state.isLogin} {...this.props} />)}
            </div>
        )  
    }
}
export default Login