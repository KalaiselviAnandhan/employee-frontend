import React from 'react' 
import {Link} from "react-router-dom"
import {Navbar, Nav, Form,FormControl,Table, Spinner} from 'react-bootstrap'
import {FaUserPlus, FaUsers,FaPowerOff,FaUser,FaUserCheck,FaUserTimes,FaHome} from 'react-icons/fa'
import ReactPaginate from 'react-paginate'
import axiosInstance from "./Api/axios"


class NavBar extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            dept:'',
            key:'',
            employeeData : [],
            loading:true,
            showEmployeeInfo:false,
            showCreateEmployee:false,
            showEmployeeHome:false,
            offset:0,
            perPage:5,
            currentPage:0,
            tableData:[]
        }
    }

    getEmployees = (keyword='') => {
        const dept=this.state.dept===''?'All':this.state.dept
        const url = (keyword==='' && this.state.dept === '') ? '/employee' :(keyword===''&&this.state.dept!=='') ? `/employee/select/${this.state.dept}` :'/employee/search/select'
        const obj = keyword===''? {headers:{'auth':localStorage.getItem('token')}}:{headers:{'auth':localStorage.getItem('token')},params:{key:keyword,department:dept}}
        axiosInstance.get(url,obj)
        .then((response)=>{
            const slice = response.data.slice(this.state.offset,this.state.offset + this.state.perPage)
            this.setState({
                employeeData:response.data,
                tableData:slice,
                pageCount:Math.ceil(response.data.length/this.state.perPage),
                loading : false
            })
        })
        .catch((error)=>{
            console.error(error)
        })
    }

    componentDidMount(){
        const value = this.props.location.pathname === '/home' ? true : false
        axiosInstance.get('/employee',{
            headers:{'auth':localStorage.getItem('token')}
        })
        .then((response)=>{
            const slice = response.data.slice(this.state.offset,this.state.offset + this.state.perPage)
            this.setState({
                employeeData:response.data,
                tableData:slice,
                pageCount:Math.ceil(response.data.length/this.state.perPage),
                showEmployeeHome:value,
                loading : false
            })
        })
        .catch((error)=>{
            console.error(error)
        })
    }

    handlePageClick = (event) =>{
        const selectedPage = event.selected
        const offset = selectedPage * this.state.perPage
        this.setState({
            currentPage:selectedPage,
            offset:offset
        },()=>{
            this.loadMoreData()
        })
    }

    loadMoreData = () => {
        const data = this.state.employeeData
        const slice = data.slice(this.state.offset,this.state.offset +this.state.perPage)
        this.setState({
            pageCount:Math.ceil(data.length/this.state.perPage),
            tableData: slice
        })
    }

    handleSelectOnChange = (event) => {
        let {name, value} = event.target
        value = (value ==='All') ? '': value
        this.setState({
            [name]:value,
            loading:true
        },() => {
            this.getEmployees()
        })
    }

    showEmpInfo = () => {
        this.setState({
            showEmployeeInfo:true,
            showCreateEmployee:false,
            showEmployeeHome:false
        })
    }

    showCreateEmp = () => {
        this.setState({
            showCreateEmployee:true,
            showEmployeeInfo:false,
            showEmployeeHome:false
        })
    }

    showHome = () =>{
        this.setState({
            showEmployeeHome:true,
            showCreateEmployee:false,
            showEmployeeInfo:false
        })
    }

    handleSearchOnChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name] : value,
            loading:true
        },()=>{
            const keyword = this.state.key
            // if(keyword!==''){
                this.getEmployees(keyword)
            // }
            // else{
            //     this.setState({
            //         loading:false
            //     })
            // }
        })
    }

    logOut = () => {
        localStorage.clear()
        this.props.history.push('/')
    }

    render(){
        return(
            <>
                <Navbar className='custom-bg' expand='lg'>
                    <Navbar.Brand><h1 className='text-white'><FaUsers className='mr-1'style={{color:'gray'}}/> Employee</h1></Navbar.Brand>
                    <Navbar.Toggle aria-controls = 'navigation' className='border-secondary'/>
                    <Navbar.Collapse id = 'navigation' className='text-left'>
                        <Nav>
                            <Link to='/home' className='nav-link'><h2 className='text-white mr-3' data-toggle='tool-tip' data-placement='bottom' title='Home' onClick={this.showHome}><FaHome className='mr-1'style={{color:'gray'}}/> Home</h2></Link>
                            <Link to='/home/add' className='nav-link'><h2 className='text-white mr-3' data-toggle='tool-tip' data-placement='bottom' title='Add New Employee' onClick={this.showCreateEmp}><FaUserPlus className='mr-1'style={{color:'gray'}}/> New</h2></Link>
                        </Nav>
                        <Form inline className='mr-auto' >
                                <FormControl className='my-2' type="text" placeholder="Search" name='key' value={this.state.key} onChange={this.handleSearchOnChange} required/>
                        </Form>
                        <Form inline className='mr-auto'>
                            <FormControl className='my-2 custom-cursor-pointer' as="select" data-toggle='tool-tip' data-placement='bottom' title='Department' name='dept' onChange={this.handleSelectOnChange}>
                                    <option>All</option>
                                    <option>Human Resource</option>
                                    <option>Finance</option>
                                    <option>Information Technology</option>
                            </FormControl>
                        </Form>
                        <div className='mr-auto'>
                            <FaPowerOff className='custom-font-size m-2 custom-cursor-pointer' data-toggle='tool-tip' data-placement='bottom' title='Log Out' onClick={this.logOut} style={{color:'gray'}}/>
                        </div>
                    </Navbar.Collapse>
                </Navbar>

                {((this.state.showCreateEmployee===false && this.state.showEmployeeInfo===false && this.state.showEmployeeHome===true ) || this.props.location.pathname==='/home')&&(this.state.loading ? 
                <Spinner animation = 'border' variant = 'info' className='mt-5 m-2 mx-auto' /> :
                <div className='col-sm-7 mx-auto custom-bg mt-4'>
                    <Table responsive="sm" className='mt-5 custom-border'>
                        <tbody>
                            {this.state.tableData.map((item)=><tr key={item.id}><td><Link to={`/home/employee/${item.id}`}><FaUser className='custom-font-size' onClick={this.showEmpInfo} data-toggle='tool-tip' data-placement='bottom' title='View Info' style={{color:'gray'}}/></Link></td><td>{item.name}</td><td>{item.designation}</td><td>{item.status?<FaUserCheck className='custom-font-size' style={{color:'#1c4926'}}/>:<FaUserTimes className='custom-font-size' style={{color:'#ff0000'}}/>}</td></tr>)}
                        </tbody>
                    </Table> 
                    {this.state.tableData.length!==0?
                    <ReactPaginate
                        className = 'm-2'
                        previousLabel={'previous'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        pageCount={this.state.pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={'pagination'}
                        subContainerClassName={'pages pagination'}
                        activeClassName={'active'}
                    />:<div className='py-5'><h6>No Employee Data!</h6></div>}
                </div>
                )}
                
            </>
        )
    }
}

export default NavBar