import React from 'react'
import {Route, Redirect} from 'react-router-dom'

function ProtectedRouter({component, ...rest}){
    let RenderComponents = component
    let hasToken = localStorage.getItem('token')
    return(
        <Route
            {...rest}
            render = {props => {
                return (hasToken !== null && hasToken !== undefined) ? (
                    <RenderComponents {...props}/>
                )
                :
                (
                    <Redirect to = {{pathname:'/'}} />
                )
            }}
        />
    )
}
export default ProtectedRouter