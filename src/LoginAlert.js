import React from 'react'
import { Button, Modal } from 'react-bootstrap'

class LoginAlert extends React.Component{

    constructor(){
        super()
        this.state = {
            showHide : false
        }
    }

    componentDidMount(){
        this.setState({ showHide: !this.state.showHide })
    }

    handleModalShowHide() {
        this.setState({ showHide: !this.state.showHide },()=>{
            if(this.props.login === true){
                this.props.history.push("/home")
            }
        })
    }

    render(){
        return(
            <div>
                <Modal show={this.state.showHide} centered>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                        <Modal.Title>Employee</Modal.Title>
                    </Modal.Header>
                    <Modal.Body><h5>{ this.props.Message }</h5></Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => this.handleModalShowHide()}>
                            Close
                        </Button>
                        <Button variant="secondary" onClick={() => this.handleModalShowHide()}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
    
}

export default LoginAlert