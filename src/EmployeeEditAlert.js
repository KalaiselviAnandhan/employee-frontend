import React from 'react'
import { Button, Modal } from 'react-bootstrap'

class ContactEditAlert extends React.Component{

    constructor(){
        super()
        this.state = {
            showHide : false
        }
    }

    componentDidMount(){
        this.setState({ showHide: !this.state.showHide })
    }

    handleModalShowHide() {
        this.setState({ showHide: !this.state.showHide },()=>{
            this.props.history.push('/')
            setTimeout(() => {
                this.props.history.push('/home')
            }, 100);
        })
    }

    render(){
        return(
            <div>
                <Modal show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                        <Modal.Title>Employee</Modal.Title>
                    </Modal.Header>
                    <Modal.Body><h5>{ this.props.editMessage }</h5></Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => this.handleModalShowHide()}>
                            Close
                        </Button>
                        <Button variant="secondary" onClick={() => this.handleModalShowHide()}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
    
}

export default ContactEditAlert