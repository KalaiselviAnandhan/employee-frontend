import React from 'react'
import axiosInstance from './Api/axios'
import CreateEmployeeAlert from './CreateEmployeeAlert'
import { ToastContainer, toast } from 'react-toastify';



class CreateEmployee extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            empid:'',
            name:'',
            age:'',
            gender:'',
            ph:'',
            email:'',
            address:'',
            city:'',
            qualification:'',
            designation:'',
            experience:'',
            salary:'',
            department:'',
            status:'',
            isError : ''
        }
    }

    handleOnchange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name] : value
        })
    }

    handleOnBlur = (event) => {
        const field = event.target.name
        if(event.target.value!==''){
            axiosInstance.get(`/validate/${event.target.name}/${event.target.value}`,{
                headers:{'auth':localStorage.getItem('token')}
            })
            .then((response)=>{
                console.log(response.data)
            })
            .catch((error)=>{
                toast.error(`${field==='empid'?'Employee Id' :field} Already Exist!`)
            })
        }
    }


    handleOnSubmit = (event) => {
        event.preventDefault()
        const formData = this.state
        axiosInstance.post(`/employee`,formData,{
            headers:{'auth':localStorage.getItem('token')}
        })
        .then((response)=>{
            this.setState({
                isError : false
            })
        })
        .catch((error)=>{
            this.setState({
                isError : true
            })
        })
    }

    render(){
        return(
            <>
                <div className = 'card col-sm-6 mx-auto mt-4 custom-bg text-left'>
                    <form onSubmit = {this.handleOnSubmit} className = 'form-group'>
                        <div className='form-row'>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Emploee Id :</label>
                                <input className = 'form-control' type = 'text' value={this.state.empid} name = 'empid'  placeholder = 'eg: EMP20HR12' onChange = {this.handleOnchange} onBlur={this.handleOnBlur} required/>
                                <ToastContainer className='custom-position'/>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Name :</label>
                                <input className = 'form-control' type = 'text' name = 'name' placeholder = 'Full Name' onChange = {this.handleOnchange} required/>
                            </div>

                            <div className="form-group col-sm-6">
                                <label className='p-1'>Age :</label>
                                <input className = 'form-control' type = 'number' name = 'age' placeholder = 'age' onChange = {this.handleOnchange} required/>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Gender :</label>
                                <select className = 'form-control' name = 'gender' onChange = {this.handleOnchange} required>
                                    <option value=''>none</option>
                                    <option value='male'>male</option>
                                    <option value='female'>female</option>
                                </select>
                            </div>

                            <div className="form-group col-sm-6">
                                <label className='p-1'>Ph :</label>
                                <input className = 'form-control' type = 'number' name = 'ph' value={this.state.ph} placeholder ='Phone Number' onChange = {this.handleOnchange} onBlur={this.handleOnBlur} required pattern = ".{10,}"/>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Email :</label>
                                <input className = 'form-control' type = 'email' name = 'email' value={this.state.email} placeholder ='Email Address' onChange = {this.handleOnchange} onBlur={this.handleOnBlur} required pattern="[a-z0-9._+%-]+@[a-z0-9]+\.[a-z]{2,}$"/>
                            </div>

                            <div className="form-group col-sm-6">
                                <label className='p-1'>Address :</label>
                                <textarea className = 'form-control' type = 'text-area' name = 'address' placeholder ='Address' onChange = {this.handleOnchange} required/>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>City :</label>
                                <input className = 'form-control' type = 'text' name = 'city' placeholder ='City' onChange = {this.handleOnchange} required/>
                            </div>
                            
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Qualification :</label>
                                <input className = 'form-control' type = 'text' name = 'qualification' placeholder ='Qualification' onChange = {this.handleOnchange} required/>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Designation :</label>
                                <input className = 'form-control' type = 'text' name = 'designation' placeholder ='Designation' onChange = {this.handleOnchange} required/>
                            </div>
                            
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Experience :</label>
                                <input className = 'form-control' type = 'number' name = 'experience' placeholder ='Experience in years' onChange = {this.handleOnchange} required/>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Salary :</label>
                                <input className = 'form-control' type = 'number' name = 'salary' placeholder ='Salary' onChange = {this.handleOnchange} required/>
                            </div>

                            <div className="form-group col-sm-6">
                                <label className='p-1'>Department :</label>
                                <select className = 'form-control' name = 'department' onChange = {this.handleOnchange} required>
                                    <option value=''>none</option>
                                    <option value='Human Resource'>Human Resource</option>
                                    <option value='Finance'>Finance</option>
                                    <option value='Information Technology'>Information Technology</option>
                                </select>
                            </div>
                            <div className="form-group col-sm-6">
                                <label className='p-1'>Status :</label>
                                <select className = 'form-control' name = 'status' onChange = {this.handleOnchange} required>
                                    <option value=''>none</option>
                                    <option value='true'>true</option>
                                    <option value='false'>false</option>
                                </select>
                            </div>

                            <button type = 'submit' className = 'btn btn-secondary mt-2'>Submit</button>
                        </div>
                    </form>
                </div>
                {this.state.isError === true ? <CreateEmployeeAlert createEmployeeMessage = 'Unable to Add! Check the Details' {...this.props} /> : (this.state.isError===false&&<CreateEmployeeAlert createEmployeeMessage = 'Employee Added!' {...this.props} />)}
            </>
        )
    }
}
export default CreateEmployee