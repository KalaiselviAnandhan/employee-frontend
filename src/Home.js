import React from 'react'
import NavBar from './NavBar'
import CreateEmployee from './CreateEmployee'
import EmployeeInfo from './EmployeeInfo'
import ProtectedRouter from './ProtectedRouter'

class Home extends React.Component{

    render(){
        return(
            <div className = "container-fluid">
                <NavBar {...this.props}/>
                <ProtectedRouter exact path='/home/add' component = {CreateEmployee} />
                <ProtectedRouter exact path='/home/employee/:employeeId' component = {EmployeeInfo} />
            </div>
        )
    }
}

export default Home