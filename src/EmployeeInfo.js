import React from "react"
import {FaPhoneAlt, FaCoins,FaAddressBook,FaBriefcase, FaCity,FaUserTie,FaUserGraduate,FaUserClock, FaEnvelope, FaUserEdit, FaUserMinus,FaIdBadge} from 'react-icons/fa'
import axiosInstance from '../employee-frontend/src/Api/axios'
import {Modal, Button} from 'react-bootstrap'
import EmployeeEditAlert from './EmployeeEditAlert'
import {Table} from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify';

class EmployeeInfo extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            show : false,
            id:'',
            empid:'',
            name:'',
            age:'',
            gender:'',
            ph:'',
            email:'',
            address:'',
            city:'',
            qualification:'',
            designation:'',
            experience:'',
            salary:'',
            department:'',
            status:'',
            isEditError : '',
            isDeleteShow : false,
            showDeleteModal : false,
        }
    }

    handleOnchange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name] : value
        })
    }

    handleHide = () => {
        this.setState({show:false})
    }

    handleModalHide = () => {
        this.setState({showDeleteModal:false})
    }

    getEmployeeInfo = () => {
        axiosInstance.get(`/employee/${this.props.match.params.employeeId}`,{
            headers:{'auth':localStorage.getItem('token')}
        })
        .then((response)=>{
            this.setState({
                id: response.data[0].id,
                empid: response.data[0].empid,
                name: response.data[0].name,
                age: response.data[0].age,
                gender: response.data[0].gender,
                ph: response.data[0].ph,
                email: response.data[0].email,
                address: response.data[0].address,
                city: response.data[0].city,
                qualification: response.data[0].qualification,
                designation: response.data[0].designation,
                experience: response.data[0].experience,
                salary: response.data[0].salary,
                department: response.data[0].department,
                status: response.data[0].status
            })
        })
        .catch((error)=>console.error(error))
    }

    handleOnBlur = (event) => {
        const field = event.target.name
        if(event.target.value!==''){
            axiosInstance.get(`/validate/${event.target.name}/${event.target.value}`,{
                headers:{'auth':localStorage.getItem('token')}
            })
            .then((response)=>{
                console.log(response.data)
            })
            .catch((error)=>{
                toast.error(`${field==='empid'?'Employee Id' :field} Already Exist!`)
                // this.setState({
                //     [field]:''
                // })
            })
        }
    }

    handleOnSubmit = (event) => {
        event.preventDefault()
        const formData = this.state
        axiosInstance.put(`/employee/${this.state.id}`,formData,{
            headers:{'auth':localStorage.getItem('token')}
        })
        .then((response) => {
            this.handleHide()
            this.setState({isEditError:false})
        })
        .catch((error)=>this.setState({isEditError:true}))
    }

    handleDelete = (event) => {
        this.setState({
            isDeleteShow : true,
            showDeleteModal : true
        })
    }

    handleModalConfirm = () => {
        const id = this.state.id
        axiosInstance.delete(`/employee/${id}`,{
            headers:{'auth':localStorage.getItem('token')}
        })
        .then((response) => {
            this.handleModalHide()
            this.props.history.push('/')
            this.props.history.push('/home')
        })
        .catch((error) => console.log(error))
    }

    componentDidMount(){
        this.getEmployeeInfo()
    }

    render(){
        
        return(
            <>
                <div className='card col-sm-6 mx-auto m-2 mt-5 mb-1 shadow p-3 custom-bg rounded'>
                    <div className='d-flex flex-row justify-content-around align-items-center'>
                        <h3>Employee Info</h3>
                        <FaUserEdit className='custom-font-size' data-toggle='tool-tip' data-placement='bottom' title='Edit Employee Info' style = {{cursor:'pointer'}} onClick={()=>this.setState({show:true})}/>
                        <FaUserMinus className='custom-font-size' data-toggle='tool-tip' data-placement='bottom' title='Delete Employee' style = {{cursor:'pointer'}} onClick = {this.handleDelete}/>
                    </div>

                    <Table responsive="sm" className='mt-2 custom-border'>
                        <tbody className='text-left'>
                            <tr><td><FaIdBadge className='custom-font-size' style={{color:'gray'}}/></td><td> Employee Id</td><td> {this.state.empid}</td></tr>
                            <tr><td><FaUserTie className='custom-font-size' style={{color:'gray'}}/></td><td> Name</td><td> {this.state.name}</td></tr>
                            <tr><td><FaUserTie className='custom-font-size' style={{color:'gray'}}/></td><td> Age</td><td> {this.state.age}</td></tr>
                            <tr><td><FaUserTie className='custom-font-size' style={{color:'gray'}}/></td><td> Gender</td><td> {this.state.gender}</td></tr>
                            <tr><td><FaPhoneAlt className='custom-font-size' style={{color:'gray'}}/></td><td> Ph</td><td> {this.state.ph}</td></tr>
                            <tr><td><FaEnvelope className='custom-font-size' style={{color:'gray'}}/></td><td> Email</td><td> {this.state.email}</td></tr>
                            <tr><td><FaAddressBook className='custom-font-size' style={{color:'gray'}}/></td><td> Address</td><td> {this.state.address}</td></tr>
                            <tr><td><FaCity className='custom-font-size' style={{color:'gray'}}/></td><td> City</td><td> {this.state.city}</td></tr>
                            <tr><td><FaUserGraduate className='custom-font-size' style={{color:'gray'}}/></td><td> Qualification</td><td> {this.state.qualification}</td></tr>
                            <tr><td><FaUserTie className='custom-font-size' style={{color:'gray'}}/></td><td> Designation</td><td> {this.state.designation}</td></tr>
                            <tr><td><FaBriefcase className='custom-font-size' style={{color:'gray'}}/></td><td> Experience</td><td> {this.state.experience}</td></tr>
                            <tr><td><FaCoins className='custom-font-size' style={{color:'gray'}}/></td><td> Salary</td><td> {this.state.salary}</td></tr>
                            <tr><td><FaCity className='custom-font-size' style={{color:'gray'}}/></td><td> Department</td><td> {this.state.department}</td></tr>
                            <tr><td><FaUserClock className='custom-font-size' style={{color:'gray'}}/></td><td> Status</td><td> {this.state.status === true ? 'Active' : 'Not Active' }</td></tr>
                        </tbody> 
                    </Table>
                </div>
                
                <Modal show={this.state.show} onHide = {this.handleHide} size = 'md' centered>
                    <Modal.Header closeButton>
                        <Modal.Title className = 'text-center'>Edit Contact Info</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit = {this.handleOnSubmit} className = 'form-group'>
                            <div className='form-row'>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Emploee Id :</label>
                                    <input className = 'form-control' type = 'text' name = 'empid' value = {this.state.empid} onChange = {this.handleOnchange} onBlur={this.handleOnBlur} required/>
                                    <ToastContainer className='custom-position'/>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Name :</label>
                                    <input className = 'form-control' type = 'text' name = 'name' value = {this.state.name}onChange = {this.handleOnchange} required/>
                                </div>

                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Age :</label>
                                    <input className = 'form-control' type = 'number' name = 'age' value = {this.state.age} onChange = {this.handleOnchange} required/>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Gender :</label>
                                    <select className = 'form-control' name = 'gender' value={this.state.gender} onChange = {this.handleOnchange} required>
                                        <option value=''>none</option>
                                        <option value='male'>male</option>
                                        <option value='female'>female</option>
                                    </select>
                                </div>

                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Ph :</label>
                                    <input className = 'form-control' type = 'number' name = 'ph' value ={this.state.ph} onChange = {this.handleOnchange} onBlur={this.handleOnBlur} required pattern = ".{10,}"/>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Email :</label>
                                    <input className = 'form-control' type = 'email' name = 'email' value ={this.state.email} onChange = {this.handleOnchange} onBlur={this.handleOnBlur} required pattern="[a-z0-9._+%-]+@[a-z0-9]+\.[a-z]{2,}$"/>
                                </div>

                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Address :</label>
                                    <textarea className = 'form-control' type = 'text-area' name = 'address' value ={this.state.address} onChange = {this.handleOnchange} required/>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>City :</label>
                                    <input className = 'form-control' type = 'text' name = 'city' value ={this.state.city}onChange = {this.handleOnchange} required/>
                                </div>
                                
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Qualification :</label>
                                    <input className = 'form-control' type = 'text' name = 'qualification' value ={this.state.qualification}onChange = {this.handleOnchange} required/>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Designation :</label>
                                    <input className = 'form-control' type = 'text' name = 'designation' value ={this.state.designation} onChange = {this.handleOnchange} required/>
                                </div>
                                
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Experience :</label>
                                    <input className = 'form-control' type = 'number' name = 'experience' value ={this.state.experience} onChange = {this.handleOnchange} required/>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Salary :</label>
                                    <input className = 'form-control' type = 'number' name = 'salary' value ={this.state.salary} onChange = {this.handleOnchange} required/>
                                </div>

                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Department :</label>
                                    <select className = 'form-control' name = 'department' value={this.state.department} onChange = {this.handleOnchange} required>
                                        <option value=''>none</option>
                                        <option value='Human Resource'>Human Resource</option>
                                        <option value='Finance'>Finance</option>
                                        <option value='Information Technology'>Information Technology</option>
                                    </select>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className='p-1'>Status :</label>
                                    <select className = 'form-control' name = 'status' value={this.state.status} onChange = {this.handleOnchange} required>
                                        <option value=''>none</option>
                                        <option value='true'>true</option>
                                        <option value='false'>false</option>
                                    </select>
                                </div>
                                <button type = 'submit' className = 'btn btn-secondary mt-2'>Submit</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                {this.state.isEditError === true ? <EmployeeEditAlert editMessage = 'Unable to Edit! Check the Details' {...this.props} /> : (this.state.isEditError===false&&<EmployeeEditAlert editMessage = 'Employee Details Updated!' {...this.props} />)}

                {this.state.isDeleteShow && 
                <Modal show={this.state.showDeleteModal}>
                    <Modal.Header closeButton onClick={this.handleModalHide}>
                        <Modal.Title>Employee</Modal.Title>
                    </Modal.Header>
                    <Modal.Body> <h5> Would You Like to Delete ?</h5> </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleModalHide}>
                            No
                        </Button>
                        <Button variant="secondary" onClick={this.handleModalConfirm}>
                            Yes
                        </Button>
                    </Modal.Footer>
                </Modal>
                }
            </>
        )
    }
}
export default EmployeeInfo