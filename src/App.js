import React from 'react'
import './App.css'
import {Route, Switch} from "react-router-dom"
import Login from "./Login"
import ProtectedRouter from './ProtectedRouter'
import Home from './Home'


function App() {
  return (
    <div className = "App">
      <Switch>
        <Route exact path ='/' component = {Login} />
        
        <ProtectedRouter  path='/home' component = {Home} />
      </Switch>
    </div>
  );
}

export default App;
